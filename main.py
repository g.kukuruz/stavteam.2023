import sys

from PyQt5 import QtWidgets

from view.app import MainApp


def application():
    app = QtWidgets.QApplication(sys.argv)
    window = MainApp()
    window.show()
    app.exec_()


if __name__ == '__main__':
    application()
