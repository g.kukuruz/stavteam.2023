/**
 * ВНИМАНИЕ! Программа содержит независимые друг от друга реализации решения задачи.
 * Решение на pyhton и решение с использованием dll.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#define MAXCHAR 1000

// расчеты зон видимости (пролетов) группировки над приемными станциями
typedef struct facility_to_satellite_s {
    char* station_name;
    char* satellite_name;
    int access;
    double start_time;
    double stop_time;
    double duration;
} facility_to_satellite_t;

// расчеты интервалов пролетов территории РФ спутниками группировки 
typedef struct russia_to_satellite_s {
    char* satellite_name;
    int access;
    int start_time;
    int stop_time;
    double duration;
} russia_to_satellite_t;

// расписание
typedef struct {
    char* satellite_name;
    int satellite_type;
    float disk_space;
    float disk_available;
    float write;
    float upload; 
} Satellite;

// выводит на экран расчеты зон видимости (пролетов) группировки
void print_facility_constellation(facility_to_satellite_t* list, int items_count) {
    for (int i = 0; i < items_count; i++) {
        facility_to_satellite_t item = list[i];
        printf("['%s', '%s', '%d', '%f', '%f', '%f']", item.station_name, item.satellite_name, item.access, item.start_time, item.stop_time, item.duration);
        printf("\n");
    }
}

// выводит на экран расчеты интервалов пролетов территории РФ спутниками
void print_russia_constellation(russia_to_satellite_t* list, int items_count) {
    for (int i = 0; i < items_count; i++) {
        russia_to_satellite_t item = list[i];
        printf("['%s', '%d', '%d', '%d', '%f']", item.satellite_name, item.access, item.start_time, item.stop_time, item.duration);
        printf("\n");
    }
}

/**
 * Функция для создания описания расписания
 * TODO: нужно решить проблемы с указателями переменных и доделать расшет размера переданных данных
*/
void generate_task(facility_to_satellite_t *facilities, int facilities_count, russia_to_satellite_t *ru_area, int ru_area_count, Satellite *satellites, int satellites_count) {
    //print_facility_constellation(facilities, facilities_count);
    //print_russia_constellation(ru_area, ru_area_count);

    for (int i = 0; i < satellites_count; i++) {
        Satellite satellite = satellites[i];
        printf("%s", satellite.satellite_name);
    }

    for (int i = 0; i < facilities_count; i++) {
        facility_to_satellite_t current_facility = facilities[i];
        // проверить съемку (здесь расчет пересечения идет правильно)
        for (int k = 0; k < ru_area_count; k++) {
            russia_to_satellite_t satellite_over_ru_area = ru_area[k];
            if (current_facility.satellite_name == current_facility.satellite_name) {
                if (current_facility.start_time > satellite_over_ru_area.stop_time) {
                    continue;
                }
                if (current_facility.stop_time < satellite_over_ru_area.start_time) {
                    continue;
                }

                // TODO: здесь нужно определить, что делает спутник (съемка, сброс данных или ожидание)
                for (int j = 0; j < satellites_count; j++) {
                    Satellite satellite = satellites[j];
                    if (satellite.satellite_name == current_facility.satellite_name) {
                        if (satellite.disk_available < satellite.disk_space) {
                            // TODO: если есть занятое место, что нужно оценить сколько еще можно снимать и когда пора сбрасывать данные
                        } else {
                            // TODO: если пустой, то должен снимать
                        }
                    }
                    printf("%s", satellite.satellite_name);
                }

                printf(
                    "Спутник %s проходит c %d по %d над территорией РФ во время видимости со станцией %s в промежуток времени с %f по %f", 
                    current_facility.satellite_name, satellite_over_ru_area.start_time, satellite_over_ru_area.stop_time, current_facility.station_name,  current_facility.start_time, current_facility.stop_time
                );
                printf("\n");
            }
        }
        if (i > 10000) { // TODO: после того, как алгоритм доделаю нужно убрать 
            break;
        }
    }
}

void load_facility(bool skip_header) 
{
    FILE *fp;

    char row[MAXCHAR];
    char *field;

    fp = fopen("csv/FacilityToSatellite.csv","r");

    // сначала считаем сколько строк в файле, чтобы создать массив
    int count = 0;
    while (feof(fp) != true)
    {
        fgets(row, MAXCHAR, fp);
        count++;
    }

    fseek(fp, 0, SEEK_SET); // переходим в начало файла  


    facility_to_satellite_t facility_array[1000];
    count = 0;
    while (feof(fp) != true)
    {
        fgets(row, MAXCHAR, fp);

        if (skip_header) {
            skip_header = false;
            continue;
        }

        int col = 0;
        char* fields[6];
        char* field = strtok(row, ",");
        char *endptr;
        
        while(field != NULL) 
        {
            fields[col] = field;
            field = strtok(NULL, ",");

            col++;
        }

        // TODO: нужно решить проблему с char*. При выводе всегда показывается последний элемент.
        facility_array[count].station_name = fields[0];
        facility_array[count].satellite_name = fields[1];
        facility_array[count].access = atoi(fields[2]);
        facility_array[count].start_time = strtod(fields[3], &endptr);
        facility_array[count].stop_time = strtod(fields[4], &endptr);
        facility_array[count].duration = strtod(fields[5], &endptr);

        //printf("%s, %s, %d, %f, %f, %f", fields[0], fields[1], atoi(fields[2]), strtod(fields[3], &endptr), strtod(fields[4], &endptr), strtod(fields[5], &endptr)); printf("\n");
        count++;
    }

    print_facility_constellation(facility_array, count);

    //free(facility_array);
}

void main() 
{
    load_facility(true);
}
