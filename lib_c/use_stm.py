from sqlalchemy import create_engine
import os, ctypes, csv

path_to_db = os.path.join(os.path.dirname(__file__) + '/../', 'database.db')
path_to_dataset = os.path.dirname(__file__) + '/../datasets/csv'

engine = create_engine("sqlite:///" + path_to_db)

stm = ctypes.cdll.LoadLibrary(os.path.dirname(__file__) + "/../bin/stm.dll")

class facility_to_satellite_s(ctypes.Structure):
    _fields_ = [
        ("station_name", ctypes.c_char_p),
        ("satellite_name", ctypes.c_char_p),
        ("access", ctypes.c_int),
        ("start_time", ctypes.c_int),
        ("stop_time", ctypes.c_int),
        ("duration", ctypes.c_float)]

class russia_to_satellite_s(ctypes.Structure):
    _fields_ = [
        ("satellite_name", ctypes.c_char_p),
        ("access", ctypes.c_int),
        ("start_time", ctypes.c_int),
        ("stop_time", ctypes.c_int),
        ("duration", ctypes.c_float)]
    
class satellite(ctypes.Structure):
    _fields_ = [
        ("satellite_name", ctypes.c_char_p),
        ("satellite_type", ctypes.c_int),
        ("disk_space", ctypes.c_float),
        ("disk_available", ctypes.c_float),
        ("write", ctypes.c_float),
        ("upload", ctypes.c_float)]
    
def read_csv(filename, skip_header = False):
    list = []
    with open(os.path.join(path_to_dataset, filename), 'r') as csvfile:
        result = csv.reader(csvfile)
        for row in result:
            if skip_header:
                skip_header = False
                continue

            if filename == "FacilityToSatellite.csv":
                facility = set_facility_to_satellite(row)
                list.append(facility)

            if filename == "RussiaToSatellite.csv":
                ru_area = set_russia_to_satellite(row)
                list.append(ru_area)

            if filename == "Satellites.csv":
                satellite = set_satellite(row)
                list.append(satellite)
            
    return list

def set_facility_to_satellite(data) -> 'facility_to_satellite_s':
    station_name = data[0]
    b_station_name = station_name.encode("utf-8")

    satellite_name = data[1]
    b_satellite_name = satellite_name.encode("utf-8")

    facility = facility_to_satellite_s()
    facility.station_name = b_station_name
    facility.satellite_name = b_satellite_name
    facility.access = int(data[2])
    facility.start_time = int(data[3])
    facility.stop_time = int(data[4])
    facility.duration = float(data[5])

    return facility

def set_russia_to_satellite(data) -> 'RussiaToSatellite':
    satellite_name = data[0]
    b_satellite_name = satellite_name.encode("utf-8")

    ru_area = russia_to_satellite_s()
    ru_area.satellite_name = b_satellite_name
    ru_area.access = int(data[1])
    ru_area.start_time = int(data[2])
    ru_area.stop_time = int(data[3])
    ru_area.duration = float(data[4])

    return ru_area

def set_satellite(data):
    satellite_name = data[0]
    b_satellite_name = satellite_name.encode("utf-8")

    satellite = satellite()
    satellite.satellite_name = b_satellite_name
    satellite.type = data[1]
    if data[1] == 1:
        satellite.disk_space = 8192 # Kinosputnik
        satellite.upload = 1
    else:
        satellite.disk_space = 4096 # Zorkiy
        satellite.upload = 0.25
    satellite.write = 4

    return satellite

def get_data(filename):
    data = read_csv(filename, True)
    items_count = len(data)

    if (items_count > 0):
        array = []
        if filename == "FacilityToSatellite.csv":
            array = (facility_to_satellite_s * items_count)()

            for i in range(items_count):
                array[i] = data[i]

        if filename == "RussiaToSatellite.csv":
            array = (russia_to_satellite_s * items_count)()

            for i in range(items_count):
                array[i] = data[i]

        if filename == "Satellites.csv":
            print(data)
            array = (satellite * items_count)()

            for i in range(items_count):
                array[i] = data[i]

        return array

#facilities = get_data("FacilityToSatellite.csv")
#ru_area = get_data("RussiaToSatellite.csv")
#satellites = get_data("Satellites.csv")

#stm.generate_task(facilities, len(facilities), ru_area, len(ru_area), satellites, len(satellites))

stm.load_facility()
