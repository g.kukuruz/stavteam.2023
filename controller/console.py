from sqlalchemy import create_engine, select
from sqlalchemy.orm import Session
from model.mapper import SatelliteMapper, SatellitePropsMapper, StationMapper
#from controller.pars_utils import parsFacility
import os


if os.name == 'nt':
    engine = create_engine('sqlite:///..\\database.db')
else:
    engine = create_engine('sqlite:///database.db')

def switch(cmd) -> None:
        ''' В зависимости от указанной команды, выполняет фукнции с переданными параметрами '''
        args = cmd.split(' ', )
        action = args[0]

        if action == 'hello':
            print(start_console.__doc__)

        if action == 'show':
            with Session(engine) as session:
                result = session.query(SatelliteMapper).first()
                print(result.id, result.name, result.type_id)

        if action == 'upload_files':
            # TODO: сделать нормальную загрузку данных
            pass

def start_console() -> None:
        ''' Ждёт ввод команд для дальнейшей обработки '''
        cmd = ''
        while cmd != 'exit':
            switch(input("enter your command:"))
        
#with Session(engine) as session:
#    result = session.query(SatelliteMapper).first()
#    print(result.id, result.name, result.type_id)
#
#    result = session.query(StationMapper).first()
#    print(result.id, result.name)
#
#    result = session.query(SatelliteMapper).filter(SatelliteMapper.id == 1).all()
#
#    for r in result:
#        print(r.id, r.name)
#
#    result = session.query(SatellitePropsMapper).all()
#
#    for r in result:
#        print(r.id, r.type, r.disk_space, r.write, r.upload)