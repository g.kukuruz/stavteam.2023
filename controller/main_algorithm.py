import sqlite3
import pandas as pd
import numpy as np
from datetime import datetime
import random
from deap import base, creator, tools, algorithms

def getPandasData(cnx, tableName):
    '''
    Функция получает на вход подключение к БД и выбирает данные из указанной таблицы.
    :param cnx: Объект подключения к БД
    :param tableName: Наименование таблицы в БД
    :return: Pandas DataFrame
    '''

    if tableName == 'satellite':
        df = pd.read_sql_query('''SELECT s.id as satellite, s.name,
                                           sp.disk_space*1024 as data_storage, 
                                           sp.write,
                                           (sp.upload * 125) as data_rate
                                    FROM satellite s
                                    left join satellite_props sp on s.type_id = sp.id 
                                    ''', cnx)
    elif tableName == 'area_target_ru':
        df = pd.read_sql_query('''SELECT  satellite_id as satellite, start_time as scan_start,
                                        stop_time as scan_end, duration as scan_duration,
                                        sp.write*125 scan_rate,
                                        sp.disk_space*1024*1024 storage_capacity,
                                        sp.write*125*duration data_volume 
                                FROM area_target_ru atr
                                left join satellite s on atr.satellite_id = s.id 
                                left join satellite_props sp on s.type_id = sp.id 
                                ''', cnx)
    elif tableName == 'facility':
        df = pd.read_sql_query('''SELECT station_id as station, start_time as transmission_start, 
                                       stop_time as transmission_end, duration as transmission_duration, 
                                       satellite_id as satellite,
                                       sp.upload*125 transmission_rate,
                                       sp.upload*125*duration transferred_data_volume
                                FROM facility f
                                left join satellite s on f.satellite_id = s.id 
                                left join satellite_props sp on s.type_id = sp.id
                                ''', cnx)
    else:
        df = pd.read_sql_query("SELECT * FROM {}".format(tableName), cnx)
    return df

def convertResult(cnx,df,resultType):
    stations = df['station'].unique()
    df = df.sort_values(by=['station', 'transmission_start'])
    df_stations = getPandasData(cnx,'station')
    df_satellits = getPandasData(cnx, 'satellite')
    try:
        for station in stations:
            filtred_df = df[df['station'] ==station]
            stationName = df_stations[df_stations['id'] == station]['name'].values[0]
            with open(stationName+'.txt','rw') as f:
                f.write(stationName)
                f.write('\n')
                f.write('-------------------------')
                f.write('\n')
                f.write('Access   *   Start Time (UTCG)   *   Stop Time (UTCG)   *   Duration(sec)   *   Sat name   *   Data (Mbytes)')
                i = 1
                for index, row in filtred_df.iterrows():
                    satelliteName = df_satellits[df_satellits['id'] == row.satellite]['name'].values[0]
                    if resultType == 'greedy':
                        f.write('       '.join([str(i), row.transmission_start.astype(str),
                                                row.transmission_end.astype(str), str(row.transmission_duration),
                                                satelliteName, str(row.actual_scanned_data_volume)]))
                    elif resultType == 'dynamic':
                        f.write('       '.join([str(i), row.transmission_start.astype(str),
                                                row.transmission_end.astype(str), str(row.transmission_duration),
                                                satelliteName, str(row.transmission_volume)]))
                    elif resultType == 'genetic':
                        f.write('       '.join([str(i), row.start_time.astype(str),
                                                row.end_time.astype(str), str(row.duration),
                                                satelliteName, str(row.transmitted_data)]))
                    i += 1
        return True
    except:
        df.to_csv('result.csv')

# Функция оценки приспособленности каждого индивидуума
def evaluate(individual):
    merged_df = pd.merge(scans_df, transmissions_df, on="satellite")
    schedules = []
    for i, satellite in enumerate(merged_df['satellite'].unique()):
        # Создание списка временных интервалов сканирования и передачи данных
        scan_windows = []
        transmission_windows = []

        # Заполнение списков временными интервалами, соответствующими текущему индивидууму
        for _, row in merged_df[merged_df['satellite'] == satellite].iterrows():
            scan_start = individual[i*2]
            scan_end = individual[i*2+1]
            transmission_start = row['transmission_start']
            transmission_end = row['transmission_end']
            if scan_start <= transmission_end and scan_end >= transmission_start:
                return 10000,   # В случае перекрытия временных интервалов сканирования и передачи данных возвращаем высокое значение
            if scan_start > scan_end:
                return 10000,   # В случае некорректного временного интервала сканирования возвращаем высокое значение
            scan_windows.append((scan_start, scan_end))
            transmission_windows.append((transmission_start, transmission_end))

        # Создание расписания работы текущего космического аппарата
        schedule = {'satellite': satellite, 'scan_windows': scan_windows, 'transmission_windows': transmission_windows}
        schedules.append(schedule)

    transmission_schedule = []
    for station in transmissions_df['station'].unique():
        transmission_windows = []
        for _, row in transmissions_df[transmissions_df['station'] == station].iterrows():
            transmission_start = row['transmission_start']
            transmission_end = row['transmission_end']
            transmission_windows.append((transmission_start, transmission_end))
        for transmission_window in transmission_windows:
            start_time = transmission_window[0]
            end_time = transmission_window[1]
            transmitted_data = 0
            for schedule in schedules:
                for scan_window in schedule['scan_windows']:
                    if scan_window[0] >= transmission_window[1]:
                        time = min(scan_window[1] - scan_window[0], (transmission_window[1] - scan_window[0]))
                        transmitted_data += time * row['scan_rate']
            duration = (end_time - start_time) / 3600
            transmission_info = {'station': station, 'satellite': schedule['satellite'], 'start_time': start_time,
                                 'end_time': end_time, 'duration': duration, 'transmitted_data': transmitted_data}
            transmission_schedule.append(transmission_info)
    return transmission_schedule,

def determine_transfer_order(max_data_matrix):
    # Используем жадный алгоритм для определения порядка передачи данных на каждом спутнике
    transfer_order = []
    for i in range(len(max_data_matrix)):
        max_data_list = [(j, max_data_matrix[i][j]) for j in range(len(max_data_matrix[i])) if max_data_matrix[i][j] > 0]
        max_data_list.sort(key=lambda x: x[1], reverse=True)
        transfer_order.append([x[0] for x in max_data_list])
    return transfer_order

# Функция моделирования работы станций методом динамического программирования
def get_schedule_dynamic(scans_df, transmissions_df):
    scans_df['scan_start'] = pd.to_datetime(scans_df['scan_start'], unit='s')
    scans_df['scan_end'] = pd.to_datetime(scans_df['scan_end'], unit='s')

    transmissions_df['transmission_start'] = pd.to_datetime(transmissions_df['transmission_start'], unit='s')
    transmissions_df['transmission_end'] = pd.to_datetime(transmissions_df['transmission_end'], unit='s')

    # Соединяем данные о сканированиях и передачах в один датасет
    merged_data = scans_df.merge(transmissions_df, on='satellite')

    # Сортируем датасет по времени начала передачи данных
    sorted_data = merged_data.sort_values(by='transmission_start')

    # Инициализируем словарь для хранения фактически отсканированных объемов данных на каждом спутнике
    scanned_data_volumes = {satellite_id: datetime.min for satellite_id in set(merged_data['satellite'])}

    # Инициализируем словарь для хранения доступного объема данных на каждом спутнике
    available_data_volumes = {satellite_id: 0 for satellite_id in set(merged_data['satellite'])}

    # Инициализируем матрицу для хранения максимального объема переданных данных для каждой пары спутник-наземная станция
    max_data_matrix = [[0 for _ in range(len(set(merged_data['station']))) ] for _ in range(len(set(merged_data['satellite'])))]

    # Проходимся по всем записям в отсортированном датасете и определяем порядок передачи данных
    for index, row in sorted_data.iterrows():
        # Получаем данные из текущей записи датасета
        start_time = row['transmission_start']
        end_time = row['transmission_end']
        satellite_id = row['satellite']
        ground_station_id = row['station']
        transmission_volume = row['transmission_duration'] * row['transmission_rate']

        # Если на космическом аппарате нет фактических данных сканирования на момент окна передачи данных на наземную станцию,
        # то передача данных не имеет смысла. Пропускаем такую запись.
        if end_time < scanned_data_volumes[satellite_id]:
            continue

        # Если бортовое запоминающее устройство космического аппарата заполнено, то космический аппарат не может вести сканирование территории.
        # Пропускаем такую запись.
        if available_data_volumes[satellite_id] == 0 and transmission_volume > 0:
            continue

        # Обновляем фактически отсканированный объем данных для спутника
        scanned_data_volumes[satellite_id] = end_time

        # Обновляем доступный объем данных для спутника
        if transmission_volume > 0:
            available_data_volumes[satellite_id] -= transmission_volume

        # Вычисляем индексы спутника и наземной станции в матрице максимального объема переданных данных
        sat_index = list(set(merged_data['satellite'])).index(satellite_id)
        ground_index = list(set(merged_data['station'])).index(ground_station_id)

        # Вычисляем максимальный объем переданных данных для текущей пары спутник-наземная станция и обновляем соответствующий элемент матрицы
        max_data_matrix[sat_index][ground_index] = max(max_data_matrix[sat_index][ground_index], transmission_volume)

    # Используя матрицу максимального объема переданных данных, определяем порядок передачи данных на каждом спутнике
    transfer_order = determine_transfer_order(max_data_matrix)
    result = pd.DataFrame(transfer_order)
    result['transmission_start'] = pd.to_datetime(result['transmission_start'], unit='s')
    result['transmission_end'] = pd.to_datetime(result['transmission_end'], unit='s')
    return result

# Функция моделирования работы станций методом генетического алгоритма
def get_schedule_genetic():
    scans_df['scan_start'] = pd.to_datetime(scans_df['scan_start'], unit='s')
    scans_df['scan_end'] = pd.to_datetime(scans_df['scan_end'], unit='s')

    transmissions_df['transmission_start'] = pd.to_datetime(transmissions_df['transmission_start'], unit='s')
    transmissions_df['transmission_end'] = pd.to_datetime(transmissions_df['transmission_end'], unit='s')

    merged_df = pd.merge(scans_df, transmissions_df, on="satellite")

    # Создание объектов для определения приспособленности и индивидуумов
    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMax)

    toolbox = base.Toolbox()

    # Определение генератора случайных чисел для начальной популяции
    toolbox.register("attr_int", random.randint, 0, 7200)

    # Определение функции для создания индивидуумов из генератора случайных чисел
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_int,
                     n=len(merged_df['satellite'].unique()) * 2)

    # Определение функции для создания начальной популяции из индивидуумов
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("evaluate", evaluate)
    # Определение оператора скрещивания
    toolbox.register("mate", tools.cxTwoPoint)
    toolbox.register("mutate", tools.mutUniformInt, low=0, up=7200, indpb=0.05)
    toolbox.register("select", tools.selTournament, tournsize=3)

    random.seed(42)

    # Создание начальной популяции
    pop = toolbox.population(n=50)

    # Определение статистик для отслеживания прогресса алгоритма
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("min", np.min)

    # Запуск генетического алгоритма
    pop, logbook = algorithms.eaSimple(pop, toolbox, cxpb=0.5, mutpb=0.2, ngen=100, stats=stats)

    # Вывод лучшего индивидуума
    best_ind = tools.selBest(pop, k=1)[0]
    transmission_schedule = evaluate(best_ind)[0]
    result = pd.DataFrame(transmission_schedule)
    result['transmission_start'] = pd.to_datetime(result['transmission_start'], unit='s')
    result['transmission_end'] = pd.to_datetime(result['transmission_end'], unit='s')
    return result

# Функция моделирования работы станций методом "жадного" алгоритма
def get_schedule_greedy():
    # объединение датасетов по полю "satellite"
    merged_df = pd.merge(scans_df, transmissions_df, on="satellite")

    # сортировка данных по времени начала сканирования территории
    merged_df = merged_df.sort_values(by=["scan_start"])

    # определение доступного объема бортового запоминающего устройства для каждого космического аппарата
    available_storage_capacity = {}
    for satellite in merged_df["satellite"].unique():
        storage_capacity = merged_df[merged_df["satellite"] == satellite]["storage_capacity"].iloc[0]
        available_storage_capacity[satellite] = storage_capacity

    # определение переменных для хранения информации о переданных данных и времени последней передачи данных с каждого космического аппарата
    transmitted_data = {}
    last_transmission_end_time = {}

    # выполнение жадной оптимизации
    result = []
    for index, row in merged_df.iterrows():
        # проверка наличия фактических данных сканирования на момент передачи данных на наземную станцию
        if row["scan_end"] < row["transmission_start"]:
            continue

        satellite = row["satellite"]
        station = row["station"]
        transmission_start = max(row["transmission_start"], last_transmission_end_time.get(satellite, 0))
        transmission_end = transmission_start + row["transmission_duration"]
        transmitted_data_volume = min((transmission_end - row["scan_start"]) * row["scan_rate"], available_storage_capacity[satellite])
        actual_scanned_data_volume = row["scan_rate"] * row["scan_duration"]

        # проверка возможности передачи данных
        if transmitted_data_volume > 0:
            result.append({
                "station": station,
                "satellite": satellite,
                "transmission_start": transmission_start,
                "transmission_end": transmission_end,
                "transmission_duration": row["transmission_duration"],
                "transmitted_data_volume": transmitted_data_volume,
                "actual_scanned_data_volume": actual_scanned_data_volume
            })
            available_storage_capacity[satellite] -= transmitted_data_volume
            last_transmission_end_time[satellite] = transmission_end

    # сортировка результата по убыванию объема переданных данных
    result = sorted(result, key=lambda x: x["transmitted_data_volume"], reverse=True)
    result = pd.DataFrame(result)
    result['transmission_start'] = pd.to_datetime(result['transmission_start'], unit='s')
    result['transmission_end'] = pd.to_datetime(result['transmission_end'], unit='s')
    return result

dt1 = datetime.now()
# Подключение к БД
cnx = sqlite3.connect('..\\database.db')

# Датафрейм расчетных интервалов времени пролета спутников над сканируемой территорией
scans_df = getPandasData(cnx, 'area_target_ru')
# Датафрейм расчетных интервалов времени прямой видимости станциями спутников
transmissions_df = getPandasData(cnx, 'facility')

# Отключаемся от БД

# Если использовать метод генетический алгоритм
# df = get_schedule_genetic()
# try:
#     convertResult(cnx, df, 'genetic')
# except:
#     pass

# Если использовать метод "жадный" алгоритм
df = get_schedule_greedy()
try:
    convertResult(cnx, df, 'greedy')
except:
    pass
# Если использовать метод динамического программирования
#df = get_schedule_dynamic()
# try:
#     convertResult(cnx, df, 'dynamic')
# except:
#     pass

# Отключаемся от БД

cnx.close()
dt2 = datetime.now()
print((dt2 - dt1).total_seconds())
