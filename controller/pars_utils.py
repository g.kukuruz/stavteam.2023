import json
from sqlalchemy import select
from sqlalchemy.orm import Session
#from controller.console import engine
from model.mapper import SatelliteMapper, StationMapper, FacilityMapper, AreaTargerRuMapper, DataOperationsMapper, SatellitePropsMapper
from sqlalchemy import create_engine
from datetime import datetime, timezone
import os


def parsAreaTarget(pathToFiles):
    '''
        Процедура парсинга файлов расписания прохода спутников над территорией РФ
        и записи данных в БД
        :param(str) pathToFiles: путь к директории с файлами данных
    '''
    if os.name == 'nt':
        # if main для того, чтобы можно было корректно выполнить парсинг из приложения
        if __name__ == "__main__":
            engine = create_engine('sqlite:///..\\database.db')
        else:
            engine = create_engine('sqlite:///database.db')
    else:
        engine = create_engine('sqlite:///database.db')

    session = Session(engine)
    session.query(AreaTargerRuMapper).delete()
    session.query(DataOperationsMapper).delete()
    session.commit()
    resultList = []
    data = {}
    sattelit = ''
    for filename in os.listdir(pathToFiles):
        if filename.endswith(".txt"):
            data = {}
            with open(os.path.join(pathToFiles, filename), 'r') as f:
                for n, line in enumerate(f, 1):
                    line = line.rstrip('\n')
                    lineList = line.split(' ', )
                    lineList = list(filter(bool, lineList))
                    if len(lineList) == 10:
                        if sattelit != '':
                            sattelitData = {
                                'Access': lineList[0],
                                'StartTime': ' '.join([lineList[1], lineList[2], lineList[3], lineList[4]]),
                                'StopTime': ' '.join([lineList[5], lineList[6], lineList[7], lineList[8]]),
                                'Duration': lineList[-1]
                            }
                            data[sattelit].append(sattelitData)
                    elif len(lineList) == 1 and not '---' in lineList[0]:
                        if sattelit != lineList[0] and sattelit != '':
                            if len(lastLine) == 10:
                                sattelitData = {
                                    'Access': lastLine[0],
                                    'StartTime': ' '.join([lastLine[1], lastLine[2], lastLine[3], lastLine[4]]),
                                    'StopTime': ' '.join([lastLine[5], lastLine[6], lastLine[7], lastLine[8]]),
                                    'Duration': lastLine[-1]
                                }
                                data[sattelit].append(sattelitData)
                        sattelit = lineList[0]
                        data[sattelit] = []
                        lastLine = lineList
            for AreaTargetInfo in data.keys():
                session = Session(engine)
                stationAndSattelitList = AreaTargetInfo.split('-To-', )
                sattelitObj = session.query(SatelliteMapper).filter(SatelliteMapper.name == stationAndSattelitList[1]).first()
                # sattelitProps = session.query(SatellitePropsMapper).filter(
                #     SatellitePropsMapper.id == sattelitObj.type_id).first()
                for AreaTarget in data[AreaTargetInfo]:
                    startTime = datetime.strptime(AreaTarget['StartTime'], '%d %b %Y %H:%M:%S.%f').replace(tzinfo=timezone.utc).timestamp()
                    endTime = datetime.strptime(AreaTarget['StopTime'], '%d %b %Y %H:%M:%S.%f').replace(tzinfo=timezone.utc).timestamp()
                    AreaTargerObj = AreaTargerRuMapper(satellite_id=sattelitObj.id,
                                           access=AreaTarget['Access'],
                                           start_time=startTime,
                                           stop_time=endTime,
                                           duration=AreaTarget['Duration'])
                    # operation = DataOperationsMapper(satellite_id=sattelitObj.id,
                    #                                  stop_time=endTime,
                    #                                  data_size=(sattelitProps.write * 125 * float(AreaTarget['Duration'])),
                    #                                  operation_type=1,
                    #                                  collect_access=AreaTarget['Access'])
                    session.add(AreaTargerObj)
                    # session.add(operation)
                session.commit()
        print('End pars file '+filename)
def parsFacility(pathToFiles):
    '''
    Процедура парсинга файлов расписания доступности станций связи для спутников
    и записи данных в БД
    :param(str) pathToFiles: путь к директории с файлами данных
    '''
    resultList = []
    data = {}
    sattelit = ''
    if os.name == 'nt':
        # if main для того, чтобы можно было корректно выполнить парсинг из приложения
        if __name__ == "__main__":
            engine = create_engine('sqlite:///..\\database.db')
        else:
            engine = create_engine('sqlite:///database.db')
    else:
        engine = create_engine('sqlite:///database.db')

    session = Session(engine)
    session.query(FacilityMapper).delete()
    session.commit()
    for filename in os.listdir(pathToFiles):
        if filename.endswith(".txt"):
            data = {}
            with open(os.path.join(pathToFiles, filename), 'r') as f:
                i = 0
                for n, line in enumerate(f, 1):
                    line = line.rstrip('\n')
                    lineList = line.split(' ', )
                    lineList = list(filter(bool, lineList))
                    if len(lineList) == 10:
                        if sattelit != '':
                            sattelitData = {
                                'Access': lineList[0],
                                'StartTime': ' '.join([lineList[1], lineList[2], lineList[3], lineList[4]]),
                                'StopTime': ' '.join([lineList[5], lineList[6], lineList[7], lineList[8]]),
                                'Duration': lineList[-1]
                            }
                            data[sattelit].append(sattelitData)
                    elif len(lineList) == 1 and not '---' in lineList[0]:
                        if sattelit != lineList[0] and sattelit != '':
                            if len(lastLine) == 10:
                                sattelitData = {
                                    'Access': lastLine[0],
                                    'StartTime': ' '.join([lastLine[1], lastLine[2], lastLine[3], lastLine[4]]),
                                    'StopTime': ' '.join([lastLine[5], lastLine[6], lastLine[7], lastLine[8]]),
                                    'Duration': lastLine[-1]
                                }
                                data[sattelit].append(sattelitData)
                        sattelit = lineList[0]
                        data[sattelit] = []
                        lastLine = lineList
            for facilityInfo in data.keys():
                session = Session(engine)
                stationAndSattelitList = facilityInfo.split('-To-', )

                sattelitObj = session.query(SatelliteMapper).filter(SatelliteMapper.name == stationAndSattelitList[1]).first()
                # sattelitProps = session.query(SatellitePropsMapper).filter(
                #     SatellitePropsMapper.id == sattelitObj.type_id).first()
                stationObj = session.query(StationMapper).filter(StationMapper.name == stationAndSattelitList[0]).first()
                if stationObj is None:
                    print('Error pars file ' + filename+'. Station '+stationAndSattelitList[0]+' not found.')
                    break
                for facility in data[facilityInfo]:
                    startTime = datetime.strptime(facility['StartTime'], '%d %b %Y %H:%M:%S.%f').replace(tzinfo=timezone.utc).timestamp()
                    endTime = datetime.strptime(facility['StopTime'], '%d %b %Y %H:%M:%S.%f').replace(tzinfo=timezone.utc).timestamp()
                    facilityObj = FacilityMapper(station_id=stationObj.id,
                                           satellite_id=sattelitObj.id,
                                           access=facility['Access'],
                                           start_time=startTime,
                                           stop_time=endTime,
                                           duration=facility['Duration'])
                    # operation = DataOperationsMapper(satellite_id=sattelitObj.id,
                    #                                  station_id=stationObj.id,
                    #                                  stop_time=endTime,
                    #                                  data_size=(sattelitProps.upload * 125 * float(
                    #                                      facility['Duration'])),
                    #                                  operation_type=0,
                    #                                  facility_access=facility['Access'])
                    session.add(facilityObj)
                    #session.add(operation)
                session.commit()
        print('End pars file ' + filename)

if __name__ == '__main__':
    dt1 = datetime.now()
    parsAreaTarget('../datasets/Russia2Constellation/')
    parsFacility('../datasets/Facility2Constellation/')
    dt2 = datetime.now()
    print((dt2-dt1).total_seconds())
