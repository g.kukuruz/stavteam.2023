import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class SatelliteMapper(Base):
    __tablename__ = "satellite"

    id = db.Column("id", db.Integer, autoincrement=True, primary_key=True)
    name = db.Column("name", db.String)
    type_id = db.Column("type_id", db.Integer)

class SatellitePropsMapper(Base):
    __tablename__ = "satellite_props"

    id = db.Column("id", db.Integer, autoincrement=True, primary_key=True)
    type = db.Column("type", db.String)
    disk_space = db.Column("disk_space", db.REAL)
    write = db.Column("write", db.REAL)
    upload = db.Column("upload", db.REAL)

class StationMapper(Base):
    __tablename__ = "station"

    id = db.Column("id", db.Integer, autoincrement=True, primary_key=True)
    name = db.Column("name", db.String)

class AreaTargerRuMapper(Base):
    __tablename__ = "area_target_ru"

    id = db.Column("id", db.Integer, autoincrement=True, primary_key=True)
    satellite_id = db.Column("satellite_id", db.Integer)
    access = db.Column("access", db.Integer)
    start_time = db.Column("start_time", db.Float)
    stop_time = db.Column("stop_time", db.Float)
    duration = db.Column("duration", db.Float)

class FacilityMapper(Base):
    __tablename__ = "facility"

    id = db.Column("id", db.Integer, autoincrement=True, primary_key=True)
    station_id = db.Column("station_id", db.Integer)
    satellite_id = db.Column("satellite_id", db.Integer)
    access = db.Column("access", db.Integer)
    start_time = db.Column("start_time", db.Float)
    stop_time = db.Column("stop_time", db.Float)
    duration = db.Column("duration", db.Float)

class DataOperationsMapper(Base):
    __tablename__ = "data_operations"

    id = db.Column("id", db.Integer, autoincrement=True, primary_key=True)
    station_id = db.Column("station_id", db.Integer)
    satellite_id = db.Column("satellite_id", db.Float)
    stop_time = db.Column("stop_time", db.Float)
    data_size = db.Column("data_size", db.Float)
    operation_type = db.Column("operation_type", db.Integer)
    collect_access = db.Column("collect_access", db.Integer)
    facility_access = db.Column("facility_access", db.Integer)
    confirmed = db.Column("confirmed", db.Integer)