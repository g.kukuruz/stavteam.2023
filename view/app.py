import os

from PyQt5 import QtWidgets

import view.design_main as design_main
import view.window_create_schedule as window_create_schedule
import view.window_display_schedule as window_display_schedule


class MainApp(QtWidgets.QMainWindow, design_main.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.btnDirCreateSchedule.clicked.connect(self.select_directory_create)
        self.btnCreateSchedule.clicked.connect(self.open_window_create_schedule)
        self.btnDirOutputData.clicked.connect(self.select_directory_display)
        self.btnOpenSchedule.clicked.connect(self.open_window_display_schedule)
        self.directory = ""
        self.value_pb = 0
        self.stations_list = ["All stations"]
        self.stations_dict = {}
        self.dates = ["All dates"]

    def select_directory_create(self):
        """Обработка кнопки выбора пути для создания расписания"""
        directory = QtWidgets.QFileDialog.getExistingDirectory(self, "Выберите директорию")
        self.pathCreateSchedule.setText(directory)
        self.directory = directory
        if self.directory:
            self.btnCreateSchedule.setEnabled(True)

    def select_directory_display(self):
        """Обработка кнопки выбора пути для открытия расписания"""
        directory = QtWidgets.QFileDialog.getExistingDirectory(self, "Выберите директорию")
        self.pathOpenSchedule.setText(directory)
        if self.pathOpenSchedule.text():
            self.btnOpenSchedule.setEnabled(True)

    def open_window_create_schedule(self):
        """Инициация и открытие окна для создания расписания"""
        self.window_create_schedule = QtWidgets.QMainWindow()
        self.ui_window_create_schedule = window_create_schedule.Ui_WindowCreateSchedule()
        self.ui_window_create_schedule.setupUi(self.window_create_schedule)
        self.window_create_schedule.show()
        self.ui_window_create_schedule.btnStartParse.clicked.connect(self.parse_for_create)
        self.ui_window_create_schedule.btnCompleteParse.clicked.connect(self.window_create_schedule.close)

    def open_window_display_schedule(self):
        """Инициация и открытие окна для показа расписания"""
        self.window_display_schedule = QtWidgets.QMainWindow()
        self.ui_window_display_schedule = window_display_schedule.Ui_WindowDisplaySchedule()
        self.ui_window_display_schedule.setupUi(self.window_display_schedule)
        self.window_display_schedule.show()
        self.parse_for_display()
        self.ui_window_display_schedule.stationsBox.addItems(self.stations_list)
        self.ui_window_display_schedule.datesBox.addItems(self.dates)
        self.ui_window_display_schedule.stationsBox.currentIndexChanged.connect(self.display_schedule)
        self.ui_window_display_schedule.datesBox.currentIndexChanged.connect(self.display_schedule)

    def parse_for_create(self):
        """Парсинг входных данных"""
        from sqlalchemy.orm import Session
        from model.mapper import SatelliteMapper, StationMapper, FacilityMapper, AreaTargerRuMapper, \
            DataOperationsMapper, SatellitePropsMapper
        from sqlalchemy import create_engine
        from datetime import datetime, timezone
        # import controller.main_algorithm as main_algorithm

        def parse_area_target(path_to_files):
            """Парсинг данных спутников"""
            engine = create_engine('sqlite:///..\\database.db')

            session = Session(engine)
            session.query(AreaTargerRuMapper).delete()
            session.query(DataOperationsMapper).delete()
            session.commit()
            sattelit = ''
            for filename in os.listdir(path_to_files):
                if filename.endswith(".txt"):
                    data = {}
                    with open(os.path.join(path_to_files, filename), 'r') as f:
                        for n, line in enumerate(f, 1):
                            line = line.rstrip('\n')
                            line_list = line.split(' ', )
                            line_list = list(filter(bool, line_list))
                            if len(line_list) == 10:
                                if sattelit != '':
                                    sattelit_data = {
                                        'Access': line_list[0],
                                        'StartTime': ' '.join([line_list[1], line_list[2], line_list[3], line_list[4]]),
                                        'StopTime': ' '.join([line_list[5], line_list[6], line_list[7], line_list[8]]),
                                        'Duration': line_list[-1]
                                    }
                                    data[sattelit].append(sattelit_data)
                            elif len(line_list) == 1 and not '---' in line_list[0]:
                                if sattelit != line_list[0] and sattelit != '':
                                    if len(last_line) == 10:
                                        sattelit_data = {
                                            'Access': last_line[0],
                                            'StartTime': ' '.join(
                                                [last_line[1], last_line[2], last_line[3], last_line[4]]),
                                            'StopTime': ' '.join(
                                                [last_line[5], last_line[6], last_line[7], last_line[8]]),
                                            'Duration': last_line[-1]
                                        }
                                        data[sattelit].append(sattelit_data)
                                sattelit = line_list[0]
                                data[sattelit] = []
                                last_line = line_list
                    for area_target_info in data.keys():
                        session = Session(engine)
                        station_and_sattelit_list = area_target_info.split('-To-', )
                        sattelit_obj = session.query(SatelliteMapper).filter(
                            SatelliteMapper.name == station_and_sattelit_list[1]).first()
                        # sattelit_props = session.query(SatellitePropsMapper).filter(
                        #     SatellitePropsMapper.id == sattelit_obj.type_id).first()
                        for area_target in data[area_target_info]:
                            start_time = datetime.strptime(area_target['StartTime'], '%d %b %Y %H:%M:%S.%f').replace(
                                tzinfo=timezone.utc).timestamp()
                            end_time = datetime.strptime(area_target['StopTime'], '%d %b %Y %H:%M:%S.%f').replace(
                                tzinfo=timezone.utc).timestamp()
                            area_target_obj = AreaTargerRuMapper(satellite_id=sattelit_obj.id,
                                                                 access=area_target['Access'],
                                                                 start_time=start_time,
                                                                 stop_time=end_time,
                                                                 duration=area_target['Duration'])
                            # operation = DataOperationsMapper(satellite_id=sattelit_obj.id,
                            #                                  stop_time=end_time,
                            #                                  data_size=(sattelit_props.write * 125 * float(
                            #                                      area_target['Duration'])),
                            #                                  operation_type=1,
                            #                                  collect_access=area_target['Access'])
                            session.add(area_target_obj)
                            # session.add(operation)
                        session.commit()
                self.ui_window_create_schedule.textEdit.append(f"File processed {filename}")
                self.value_pb += percent
                self.ui_window_create_schedule.progressBar.setValue(round(self.value_pb))

        def parse_facility(path_to_files):
            """Парсинг данных станций"""
            engine = create_engine('sqlite:///..\\database.db')

            session = Session(engine)
            session.query(FacilityMapper).delete()
            session.commit()
            sattelit = ''
            for filename in os.listdir(path_to_files):
                if filename.endswith(".txt"):
                    data = {}
                    with open(os.path.join(path_to_files, filename), 'r') as f:
                        i = 0
                        for n, line in enumerate(f, 1):
                            line = line.rstrip('\n')
                            line_list = line.split(' ', )
                            line_list = list(filter(bool, line_list))
                            if len(line_list) == 10:
                                if sattelit != '':
                                    sattelit_data = {
                                        'Access': line_list[0],
                                        'StartTime': ' '.join([line_list[1], line_list[2], line_list[3], line_list[4]]),
                                        'StopTime': ' '.join([line_list[5], line_list[6], line_list[7], line_list[8]]),
                                        'Duration': line_list[-1]
                                    }
                                    data[sattelit].append(sattelit_data)
                            elif len(line_list) == 1 and not '---' in line_list[0]:
                                if sattelit != line_list[0] and sattelit != '':
                                    if len(last_line) == 10:
                                        sattelit_data = {
                                            'Access': last_line[0],
                                            'StartTime': ' '.join(
                                                [last_line[1], last_line[2], last_line[3], last_line[4]]),
                                            'StopTime': ' '.join(
                                                [last_line[5], last_line[6], last_line[7], last_line[8]]),
                                            'Duration': last_line[-1]
                                        }
                                        data[sattelit].append(sattelit_data)
                                sattelit = line_list[0]
                                data[sattelit] = []
                                last_line = line_list
                    for facility_info in data.keys():
                        session = Session(engine)
                        station_and_sattelit_list = facility_info.split('-To-', )

                        sattelit_obj = session.query(SatelliteMapper).filter(
                            SatelliteMapper.name == station_and_sattelit_list[1]).first()
                        # sattelit_props = session.query(SatellitePropsMapper).filter(
                        #     SatellitePropsMapper.id == sattelit_obj.type_id).first()
                        station_obj = session.query(StationMapper).filter(
                            StationMapper.name == station_and_sattelit_list[0]).first()
                        if station_obj is None:
                            print('Error pars file ' + filename + '. Station ' + station_and_sattelit_list[
                                0] + ' not found.')
                            break
                        for facility in data[facility_info]:
                            start_time = datetime.strptime(facility['StartTime'], '%d %b %Y %H:%M:%S.%f').replace(
                                tzinfo=timezone.utc).timestamp()
                            end_time = datetime.strptime(facility['StopTime'], '%d %b %Y %H:%M:%S.%f').replace(
                                tzinfo=timezone.utc).timestamp()
                            facility_obj = FacilityMapper(station_id=station_obj.id,
                                                          satellite_id=sattelit_obj.id,
                                                          access=facility['Access'],
                                                          start_time=start_time,
                                                          stop_time=end_time,
                                                          duration=facility['Duration'])
                            # operation = DataOperationsMapper(satellite_id=sattelit_obj.id,
                            #                                  station_id=station_obj.id,
                            #                                  stop_time=end_time,
                            #                                  data_size=(sattelit_props.upload * 125 * float(
                            #                                      facility['Duration'])),
                            #                                  operation_type=0,
                            #                                  facility_access=facility['Access'])
                            session.add(facility_obj)
                            # session.add(operation)
                        session.commit()
                self.ui_window_create_schedule.textEdit.append(f"File processed {filename}")
                self.value_pb += percent
                self.ui_window_create_schedule.progressBar.setValue(round(self.value_pb))
            # TODO: если нижние две команды не успевают обрабатываться, то можно их удалить, либо удалить TODO
            self.ui_window_create_schedule.textEdit.append("Compiling the schedule, please wait")
            self.ui_window_create_schedule.progressBar.setValue(99)

        try:
            self.ui_window_create_schedule.btnStartParse.setEnabled(False)
            if "Facility" in os.listdir(self.pathCreateSchedule.text())[0]:
                dir_facility = self.directory + "/" + os.listdir(self.pathCreateSchedule.text())[0]
                dir_russia = self.directory + "/" + os.listdir(self.pathCreateSchedule.text())[1]
            else:
                dir_facility = self.directory + "/" + os.listdir(self.pathCreateSchedule.text())[1]
                dir_russia = self.directory + "/" + os.listdir(self.pathCreateSchedule.text())[0]

            len_dir_facility = len(os.listdir(dir_facility))
            len_dir_russia = len(os.listdir(dir_russia))
            percent = 100 / (len_dir_facility + len_dir_russia)

            self.ui_window_create_schedule.textEdit.append("Processing of input data:")
            parse_area_target(dir_russia)
            parse_facility(dir_facility)
            # TODO: тут прописать или раскомментировать функцию для создания расписания // main_algorithm уже импортирован
            import controller.main_algorithm
            self.ui_window_create_schedule.progressBar.setValue(100)
            self.ui_window_create_schedule.textEdit.append("\nReady!")
            self.ui_window_create_schedule.textEdit.append("Thanks for waiting, your files are saved in <here path>")
            self.ui_window_create_schedule.btnCompleteParse.setEnabled(True)
        except BaseException as e:
            print(e)
            error = QtWidgets.QMessageBox()
            error.setWindowTitle("An error has occurred")
            error.setText("Check the path to your data")
            error.setIcon(QtWidgets.QMessageBox.Warning)

            btnOk = error.addButton(QtWidgets.QMessageBox.Ok)
            btnOk.clicked.connect(self.popup_action_create)

            error.exec_()

    def parse_for_display(self):
        """Парсинг данных расписания для корректного отображения"""
        # TODO: переделать блок try под корректные данные
        # Названия станций должны быть добавлены в список self.stations_list
        # Даты в обычном текстовом формате "%d %m %Y" должны быть добавлены в список self.dates
        # Словарь self.stations_dict должен быть в формате <название станции>: <путь к файлу со станцией>
        try:
            if "facility" not in self.pathOpenSchedule.text().lower():
                raise ValueError("Wrong path")
            dates_flag = True
            for filename in os.listdir(self.pathOpenSchedule.text()):
                self.stations_list.append(filename[9:-4])
                self.stations_dict[filename[9:-4]] = os.path.join(self.pathOpenSchedule.text(), filename)
                if dates_flag:
                    with open(os.path.join(self.pathOpenSchedule.text(), filename)) as f:
                        for row in f.readlines():
                            row.strip()
                            row_split = row.split()
                            if len(row_split) == 10:
                                date = " ".join(row_split[1:4])
                                if date not in self.dates:
                                    self.dates.append(date)
                    dates_flag = False
        except BaseException as e:
            print(e)
            error = QtWidgets.QMessageBox()
            error.setWindowTitle("An error has occurred")
            error.setText("Check the path to your data")
            error.setIcon(QtWidgets.QMessageBox.Warning)

            btnOk = error.addButton(QtWidgets.QMessageBox.Ok)
            btnOk.clicked.connect(self.popup_action_display)

            error.exec_()

    def display_schedule(self):
        """Отображение расписания"""
        self.ui_window_display_schedule.listWidget.clear()
        if self.ui_window_display_schedule.stationsBox.currentText() == "All stations" and self.ui_window_display_schedule.datesBox.currentText() == "All dates":
            for filename in self.stations_dict.values():
                with open(filename) as f:
                    for row in f.readlines():
                        self.ui_window_display_schedule.listWidget.addItem(row)
        elif self.ui_window_display_schedule.stationsBox.currentText() == "All stations" and self.ui_window_display_schedule.datesBox.currentText() != "All dates":
            for filename in self.stations_dict.values():
                with open(filename) as f:
                    for row in f.readlines():
                        row_split = row.strip().split()
                        if len(row.split()) != 10:
                            self.ui_window_display_schedule.listWidget.addItem(row)
                        elif self.ui_window_display_schedule.datesBox.currentText() == " ".join(row_split[1:4]):
                            self.ui_window_display_schedule.listWidget.addItem(row)
        elif self.ui_window_display_schedule.stationsBox.currentText() != "All stations" and self.ui_window_display_schedule.datesBox.currentText() == "All dates":
            with open(self.stations_dict[self.ui_window_display_schedule.stationsBox.currentText()]) as f:
                for row in f.readlines():
                    self.ui_window_display_schedule.listWidget.addItem(row)
        else:
            with open(self.stations_dict[self.ui_window_display_schedule.stationsBox.currentText()]) as f:
                for row in f.readlines():
                    row_split = row.strip().split()
                    if len(row.split()) != 10:
                        self.ui_window_display_schedule.listWidget.addItem(row)
                    elif self.ui_window_display_schedule.datesBox.currentText() == " ".join(row_split[1:4]):
                        self.ui_window_display_schedule.listWidget.addItem(row)

    def popup_action_create(self):
        """Обработка при ошибке создания расписания"""
        self.window_create_schedule.close()

    def popup_action_display(self):
        """Обработка при ошибке открытия и показа расписания"""
        self.ui_window_display_schedule.stationsBox.clear()
        self.ui_window_display_schedule.datesBox.clear()
        self.stations_list = ["All stations"]
        self.dates = ["All dates"]
        self.window_display_schedule.close()
