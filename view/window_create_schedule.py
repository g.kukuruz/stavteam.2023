from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_WindowCreateSchedule(object):
    def setupUi(self, WindowCreateSchedule):
        WindowCreateSchedule.setObjectName("WindowCreateSchedule")
        WindowCreateSchedule.resize(500, 300)
        WindowCreateSchedule.setMinimumSize(QtCore.QSize(500, 300))
        WindowCreateSchedule.setMaximumSize(QtCore.QSize(500, 300))
        self.layoutWidget = QtWidgets.QWidget(WindowCreateSchedule)
        self.layoutWidget.setGeometry(QtCore.QRect(9, 266, 481, 25))
        self.layoutWidget.setObjectName("layoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btnStartParse = QtWidgets.QPushButton(self.layoutWidget)
        self.btnStartParse.setObjectName("btnStartParse")
        self.horizontalLayout.addWidget(self.btnStartParse)
        self.btnCompleteParse = QtWidgets.QPushButton(self.layoutWidget)
        self.btnCompleteParse.setEnabled(False)
        self.btnCompleteParse.setObjectName("btnCompleteParse")
        self.horizontalLayout.addWidget(self.btnCompleteParse)
        self.widget = QtWidgets.QWidget(WindowCreateSchedule)
        self.widget.setGeometry(QtCore.QRect(10, 10, 481, 251))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.textEdit = QtWidgets.QTextEdit(self.widget)
        self.textEdit.setReadOnly(True)
        self.textEdit.setObjectName("textEdit")
        self.textEdit.append("Freezes are possible during the execution of the program")
        self.textEdit.append("To avoid errors, please do not close or minimize the window until the program is fully executed.\n")
        self.verticalLayout.addWidget(self.textEdit)
        self.progressBar = QtWidgets.QProgressBar(self.widget)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName("progressBar")
        self.verticalLayout.addWidget(self.progressBar)

        self.retranslateUi(WindowCreateSchedule)
        QtCore.QMetaObject.connectSlotsByName(WindowCreateSchedule)

    def retranslateUi(self, WindowCreateSchedule):
        _translate = QtCore.QCoreApplication.translate
        WindowCreateSchedule.setWindowTitle(_translate("WindowCreateSchedule", "Creating a schedule"))
        self.btnStartParse.setText(_translate("WindowCreateSchedule", "Start"))
        self.btnCompleteParse.setText(_translate("WindowCreateSchedule", "Ready"))
